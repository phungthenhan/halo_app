import { Controller, Get , Req, Post, HttpCode, Header, Param,Body, HttpException, HttpStatus ,UseGuards } from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import test from './test';
import { CreateCatDto } from './create-cat.dto';


@Controller('user')
export class UserController {

  @Post('hhr')
  async create(@Body() createCatDto: CreateCatDto) {
    console.log(createCatDto);
    throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    return 'This action adds a new cat';
  }

  // @Get()
  // findAll(@Req() request ) {
  //   return {nhanpt: 'stringify', test: 'van van', thong: 'tinh tu' , req: request.id };
  // }

  @Get('ab_cd')
  @Header('Cache-Control', 'nhanpt')
  @UseGuards(AuthGuard())
  findAll() {
    return 'This route uses a wildcard';
  }

  // @Get(':test')
  // findOne(@Param('test') id) {
  //   console.log(id);
  //   return `This action returns a #${id} cat`;
  // }

  @Get()
  async findAll1(): Promise<test[]> {
    return [{name:'here',age:12}];
  }

  @Get('nhanpt')
  async test(): Promise<any[]> {
    return [];
  }
}
