export interface ICustomer{
  readonly name:string;
  readonly age:string;
  readonly email:string;
  readonly country:string;
  readonly phone:string;
}

export interface JwtPayload {
  email: string;
}