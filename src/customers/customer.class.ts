export class Customer{
  readonly name:string;
  readonly age:string;
  readonly email:string;
  readonly country:string;
  readonly phone:string;
  constructor(name:string, age:string,email:string, phone:string){
    this.name=name;
    this.age=age;
    this.email=email;
    this.phone=phone;
  }
}