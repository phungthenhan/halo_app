import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './users/user.controller';
import { UsersService } from './users/users.service';
import { CustomersController } from './customers/customers.controller';
import { CustomersService } from './customers/customers.service';
import { CustomersModule } from './customers/customers.module';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [CustomersModule, AuthModule],
  controllers: [AppController, UserController, CustomersController],
  providers: [AppService, UsersService, CustomersService, AuthService],
})
export class AppModule {}
